import React, { useState,useEffect} from "react";
// import React from 'react';
import { fade,makeStyles, useTheme,Theme } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import ClearIcon from '@material-ui/icons/Clear';
import InputBase from '@material-ui/core/InputBase';
import SearchIcon from '@material-ui/icons/Search';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import {LIST_DEVICES} from '../../constants';
import UpdateDevice from '../UpdateDevice';
import {UPDATE_DEVICES,VEHICLE_TYPE} from '../../constants'

import axios from 'axios'
import {
  Grid,
  Paper,
  TableContainer,
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  TablePagination,
  withStyles,
} from "@material-ui/core";
import { MoreVert } from "@material-ui/icons";
import styles from './Admin.style';
import MiniDrawer from "../Tracking";
// import Header from '../layouts/Header'
import AddIcon from '@material-ui/icons/Add';
import Sidenav from "../Sidebar";
import { NavLink ,Link} from "react-router-dom";
import Axios from "axios";
function ViewSales(props) {
  const { classes } = props;
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(5);
  const [rows, setRows] = React.useState([]);
  const [rowCount, setRowCount] = useState("");
  const [list,setList]=useState([]);
  const [show,setShow]=useState(false);
  const [id,setId]=useState('');
  const [name,setName]=useState('');
  const [type,setType]=useState('');
  const [key,setKey]=useState('');
  const [data,setData]=useState([])

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };
   useEffect(() => {
     let token=localStorage.getItem("token");
     if(token){
      axios.get(LIST_DEVICES,{
        headers:{"Authorization":`Token ${token}`}
      })
      .then(resp=>{
        console.log(resp.data,"list")
        setList(resp.data)
      })
     }    
   }, [])
   let deleteHandler=(id)=>{
    axios.delete(`${UPDATE_DEVICES}/${id}/`)
    .then(resp=>{
      console.log(resp.data);
      window.location.reload(false)
    })
   }
   let searchHandler=(e)=>{
      let val=e.target.value;
     let data=list.filter(i=>{
       let value=val.toLowerCase();
       let dev=i.device_id.toLowerCase();
       return dev.includes(value)
     })
     if(data.length>0){
     setList(data) 
     }
   }
   let editHandler=(key,id,name,type)=>{ 
     alert("Are You Sure To Edit The Fields")    
    console.log(id,name,type)
    setShow(true);
    setId(id);
    setName(name);
    setType(type);
    setKey(key)
   }
   React.useEffect(()=>{
    Axios.get(VEHICLE_TYPE)
    .then(resp=>{
      console.log(resp.data)
      setData(resp.data)
    })
  },[])
  return (
    <div style={{ background: '#EFEFEF' }}>
      <Sidenav/>
      <div style={{ marginTop: "145px ", marginLeft: '100px', PaddingBottom: "20px" }}>
        <div className={classes.managebtn}>
          <Button variant="contained" className={classes.mbnt} >Device Management</Button>
         <Link to="/Accountmanage"><Button variant="contained" className={classes.abnt}><span style={{textDecoration:"none"}}>Account&nbsp;Management</span></Button></Link>
          <div className={classes.searchIcons}>
              <SearchIcon />
            </div>
            <InputBase
              placeholder="  Search…"
              onChange={searchHandler}
              classes={{
                root: classes.inputRoots,
                input: classes.inputInputs,
              }}
              inputProps={{ 'aria-label': 'search' }}
            />
            <div className={classes.drop}>
            <div class="form-control"  style={{marginLeft:"25px"}}>
              
  <input id="my-input" aria-describedby="my-helper-text" name="All" style={{width:"276px",height:"38px",border:"1px solid #BDC3C5",borderRadius:"5px"}} />   
</div>
<div class="form-control" style={{marginLeft:"820px",marginTop:"-154px"}}>
  {data.map(i=>(
    <React.Fragment>
      
    </React.Fragment>
  ))}
  <select name="cars" id="cars" label="All" style={{width:"182px",height:"38px",border:"1px solid #BDC3C5",borderRadius:"5px"}}>
  <option value="">All</option>    
    {data.map(i=>(
      <React.Fragment>
    <option value={i}>{i}</option>    
      </React.Fragment>
    ))}
    
  </select>  
</div>
    
      </div>
      <NavLink to="/adddevices">
          <Button className={classes.add}>
              <AddIcon style={{color:'white'}}/>
              Add Device
            </Button></NavLink>
            <NavLink to="/Map"><Button className={classes.clear}>
              <ClearIcon/>
            </Button></NavLink>
        </div>
        <Grid container spacing={0}>
          <img className={classes.ad} src={require('./Admin1.png')} />
          <h1 className={classes.adtxt}>Admin</h1>
          <Grid item xs={12} sm={12} md={12} lg={12}>
            <Paper className={classes.root} style={{ width: "98%",height:'100%' }}>
              <TableContainer className={classes.container}>
                <Table stickyHeader aria-label="sticky table">
                  <TableHead>
                    <TableRow>
                      <TableCell className={classes.label}>S.No </TableCell>
                      <TableCell className={classes.label}>
                        Icon
                      </TableCell>
                      <TableCell style={{ marginLeft: '100px' }} className={classes.label}>
                        Device ID
                      </TableCell>
                      <TableCell className={classes.label}>
                        Device Name
                      </TableCell>
                      <TableCell>
                      </TableCell>
                      <TableCell>
                      </TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {/* {rows
                      .slice(
                        page * rowsPerPage,
                        page * rowsPerPage + rowsPerPage
                      )

                      .map((row, index) => { */}
                    {/* let slno = rowsPerPage * page;

                        return ( */}
                    {list.map((i,index)=>(
                      <React.Fragment>
                        <TableRow>
                      <TableCell
                        className={classes.rowData}
                        classes={{ root: classes.rootpadding }}
                      >
                        {index+1}
                      </TableCell>
                      <TableCell className={classes.rowData}>
                      <img  src={require('./van.png')} />
                      </TableCell>
                      <TableCell className={classes.rowData}>
                      {i.device_id}
                      </TableCell>
                    <TableCell className={classes.rowData}>{i.device_name}</TableCell>
                      <TableCell>
                      <Button variant="contained" className={classes.edbtn} style={{paddingBottom:'20px'}} >
                      <p style={{position:'absolute',top:'-10px'}} onClick={e=>editHandler(i.id,i.device_id,i.device_name,i.vehicel_type)}>Edit</p>
                        </Button>
                      </TableCell>
                      <TableCell>
                        <Button  variant="contained"  className={classes.dltbtn} onClick={e=>deleteHandler(i.id)}>
                          <p style={{position:'absolute',top:'-10px'}}>Delete</p>
                          </Button>
                      </TableCell>
                    </TableRow>
                      </React.Fragment>
                    ))}
                   
                     
                   
                   
                    <TableRow></TableRow>
                    {/* ); })} */}
                  </TableBody>
                </Table>
              </TableContainer>
              <TablePagination
                rowsPerPageOptions={[5, 10, 25, 50]}
                component="div"
                count={rowCount}
                rowsPerPage={rowsPerPage}
                page={page}
                onChangePage={handleChangePage}
                onChangeRowsPerPage={handleChangeRowsPerPage}
              />
            </Paper>
          </Grid>
        </Grid>
      </div>
    <UpdateDevice keys={key} open={show} ids={id} nam={name} typ={type}/>
    </div>
  );
}

export default withStyles(styles)(ViewSales);
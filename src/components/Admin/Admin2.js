import React, { useState } from "react";
import { fade, makeStyles, useTheme } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import ClearIcon from '@material-ui/icons/Clear';
import InputBase from '@material-ui/core/InputBase';
import SearchIcon from '@material-ui/icons/Search';
import styles from './Admin2.style';
import {
  Grid,
  Paper,
  TableContainer,
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  TablePagination,
  withStyles,
  Card,
  Typography,
  TextField,
} from "@material-ui/core";
import { MoreVert } from "@material-ui/icons";
import MiniDrawer from "../Tracking";
import AddIcon from '@material-ui/icons/Add';
import Sidenav from "../Sidebar";
import { NavLink } from "react-router-dom";
function Accountmanage(props) {
  const { classes } = props;
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(5);
  const [rows, setRows] = React.useState([]);
  const [rowCount, setRowCount] = useState("");
  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };
  return (
    <div style={{ background: '#EFEFEF' }}>
      <Sidenav />
      <div style={{ marginTop: "145px ", marginLeft: '100px', PaddingBottom: "20px" }}>
        <div className={classes.managebtn}>
          <NavLink to="/ViewSales"><Button variant="contained" style={{ textDecoration: 'none' }} className={classes.mbtn} >Device Management</Button></NavLink>
          <Button variant="contained" style={{textDecoration:'none'}} className={classes.abtn} > Account&nbsp;Management</Button>
          <NavLink to="/Map"><Button variant="contained" className={classes.clr}>
            <ClearIcon />
          </Button></NavLink>
        </div>
        <Grid container spacing={0}>
          <img className={classes.ad} src={require('./Admin1.png')} />
          <h1 className={classes.adtxt}>Admin</h1>
          <Grid>
          </Grid>
        </Grid>
        <Grid></Grid>
        <Grid>
          <Paper className={classes.root} style={{ width: "98%", height: "400px" }}>
            <Typography className={classes.t1}>* Business Name/Individual Name</Typography>
            <input id="my-input" aria-describedby="my-helper-text" className={classes.bn} variant="outlined" focused={false} margin="dense"/>
         <Typography className={classes.t4}>Address</Typography>
         <input id="my-input" aria-describedby="my-helper-text"  className={classes.adr} variant="outlined" focused={false} margin="dense"/>
            <Grid item xs={12} sm={12} md={12} lg={12}>
              <Typography className={classes.t2}>GST Number</Typography>
              <input id="my-input" aria-describedby="my-helper-text"  className={classes.gn} variant="outlined" focused={false} margin="dense"/>
              <Grid item xs={12} sm={12} md={12} lg={12}>
                <Typography className={classes.t3}>Phone Number</Typography>
                <input id="my-input" aria-describedby="my-helper-text"  className={classes.pn} variant="outlined" margin="dense" focused={false}/>
                <Button variant="contained" className={classes.save}>save</Button>
              </Grid>
            </Grid>
          </Paper>
        </Grid>
      </div>
    </div>
  );
}
export default withStyles(styles)(Accountmanage);
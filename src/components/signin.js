import React, { Component } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Typography, Card, TextInput, TextField, responsiveFontSizes, Button, Divider, CardMedia } from '@material-ui/core';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Grid from '@material-ui/core/Grid';
import './signin.css';
import axios from 'axios';
import { LOGIN_URL,USER_ID } from '../constants'
import { Link, NavLink } from 'react-router-dom';
import SimpleModal from './PopUp';
// import backgrd './assets/backgrd.png';

const useStyles = {
  media: {
    height: 0,
    paddingTop: '56', // 16:9,
    marginTop: '30',
    width: '10px',
    color: 'white'
  },
  card: {

    textAlign: 'center',
  },
  inbox: {
    textAlign: 'center',
    marginLeft: '28',
    color: 'red',
    backgroundColor: 'red'
  },
};
export default class Login extends Component {

  constructor(props) {
    super(props)
    this.state = {
      email: '', password: '', show: false, error: ''

    }
  }

  handleChange = (e) => {
    this.setState({ [e.target.name]: e.target.value })
    if (e.target.name == "password") {
      let regPass = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$/;
      let passValid = regPass.test(e.target.value);
      if (passValid == false) {
        this.setState({ error: "min 8 char should have 1 upper,lower case and one number" })
        console.log(passValid)
      }
      else {
        this.setState({ error: '' })
      }

    }
  }
  rememberHandler = (e) => {
    console.log(e.target.checked);
    if (e.target.checked) {
      localStorage.setItem("email", this.state.email);
      localStorage.setItem("password", this.state.password)
    }
  }
  componentDidMount() {
    let email = localStorage.getItem("email");
    let password = localStorage.getItem("password");
    this.setState({
      email: email,
      password: password
    })
  }
  devicesHandler=(token)=>{  
    axios.get("https://apeiron-mobility.herokuapp.com/owned-devices/",{
      headers:{Authorization:`Token ${token}`}
    })
    .then(resp=>{
      let data=resp.data[0].relations;
      console.log(data,"devices"); 
      if(resp.data[0].relations.length>0){
        this.props.history.push("/map")        
      } 
      else{
        this.props.history.push("/track")      
      }          
      
    })
    .catch(error=>{
      alert(error)
    })
    }
  loginHandler = (e) => {
    e.preventDefault();
    let data = {
      "email": this.state.email,
      "password": this.state.password
    }
    axios.post(LOGIN_URL, data)
      .then(resp => {
        console.log(resp.data,"data")
        this.setState({ show: true })
        let token=resp.data.key;
        localStorage.setItem("token",token)
        this.devicesHandler(token)
        if(token){
          axios.get("https://apeiron-mobility.herokuapp.com/user-id/",
        {headers:{'Authorization':`Token ${token}`}})
        .then(resp=>{
          console.log(resp.data)
          localStorage.setItem("user_id",resp.data)
        }
         ) 
         .catch(error=>{
           this.setState({error:"Something went wrogn,please try agin"})
         }) 
        }              
      })  
      .catch(error => {
        // alert(error)
        if (error == "Error: Request failed with status code 400") {
          this.setState({ error: "Unable to log in with provided credentials." })
        }
        if (error == "Error: Network Error") {
          this.setState({ error: "check your internet connection" })
        }
      })
  }
  render() {
    const { classes } = this.props;
    return (
      <div>
        <Grid item xs={12}>
          <div className="main">
            {/* <img className="email" src={'../assets/mail.png'} /> */}
            <Grid container>
              <Grid item xs={4} className="red" justify="right" color="red" spacing="3"></Grid>
              <Grid item justify="center" color="green" direction="column" xs={4}>
                <div>
                  <Card className="card">
                    <form>
                      <img
                        className="mail"
                        src={require('../assets/mail.png')}
                      />
                      <Typography className="mail-txt">contact us</Typography>
                      <img
                        className="media"
                        src={require('../assets/logo.png')}
                      />

                      <Typography className="Text-input">Welcome back</Typography>
                      <Typography className="sm-txt">Sign In, to continue</Typography>
                      <TextField
                        id="outlined-basic"
                        type="email"
                        name="email"
                        tabindex="1"
                        value={this.state.email}
                        onChange={this.handleChange}
                        variant="outlined"
                        margin="dense"
                        placeholder="Enter your email"
                        focused={false}
                        className="inbox">
                      </TextField>
                      <div className="msg"></div>
                      <TextField
                        id="outlined-basic"
                        type="password"
                        name="password"
                        value={this.state.password}
                        onChange={this.handleChange}
                        variant="outlined"
                        className="box-1"
                        placeholder="Enter your password"
                        focused={false}
                        margin="dense"
                        className="box">

                      </TextField>
                      <div className="lck"></div>
                      <div className="check">
                        <Grid container direction="row">
                          {/* <FormControlLabel
                      control={
                         <Checkbox
                         color="secondary"
                        name="checkedI"
                        // className="checkin"
                        
                      />}
                      placeholder="Remember me"
                      />                                        */}
                          <Checkbox className="check" onChange={this.rememberHandler} inputProps={{ 'aria-label': 'uncontrolled-checkbox' }} />
                          <Typography className="chck-txt">Remember&nbsp;me</Typography>
                          <NavLink to="/forgot" style={{ textDecoration: "none" }}><Typography className="forgt" >Forgot&nbsp;Password?</Typography></NavLink>
                          {/* <NavLink to="/forgot"> <Typography className="forgt">ForgotPassword?</Typography></NavLink> */}
                        </Grid>
                      </div>
                      {this.state.error &&
                        <div>
                          <span className="error">{this.state.error}</span>
                        </div>}
                      <Button variant="contained" className="button" onClick={this.loginHandler}>
                        Sign In
                      </Button>
                      <Grid container direction="row">

                        <Divider className="hr-text" />
                        <b className="bold">Or</b>
                        <Divider className="hr" />
                      </Grid>
                      <Grid
                        direction="row"
                        justify="center"
                      >
                        {/* <Button variant="contained" className="g-btn">
                      Google
                      </Button>
                      <Button variant="contained" className="f-btn">
                      facebook
                      </Button> */}
                      </Grid>
                      <Typography>
                        <p className="Nw">New user?</p>
                      </Typography>
                      <Link to="/register" style={{ textDecoration: "none" }}><Typography className="acnt"><b>CreateAccount</b></Typography></Link>

                    </form>
                  </Card>
                </div>
              </Grid>
              <Grid item xs={12} sm={4}>

              </Grid>
            </Grid>
          </div>
        </Grid>
        <div>
        <img className="purview-s" src={require('../assets/purview.png')}/>
        <Typography className="purview-logo">powered by</Typography>
        </div>
      </div>
    )
  }
}
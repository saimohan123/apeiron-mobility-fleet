import React from "react";
import { subscribe } from "mqtt-react";
import { divIcon } from "leaflet";
import { renderToStaticMarkup } from "react-dom/server"
import { Map as LeafletMap, TileLayer, Marker, Popup,Polyline } from "react-leaflet";
import Axios from "axios";
import MiniDrawer from "./Tracking";
import axios from 'axios';
import {OWNED_DEVICES} from '../constants'
import bike from '../assets/vehicle.svg'
import { Typography } from "@material-ui/core";

class Map extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      lat: "12.996041",
      log: "77.697487",
      newdata:[],
      devices:[]   
    };
  }
  static getDerivedStateFromProps(nextProps,prevState){
    console.log(nextProps ,"nextProps")
    if(nextProps.data.length>0){
      console.log(nextProps.data,"nextProps");
      let key=nextProps.data;
      let duplicates = key.filter( (ele, ind) => ind === key.findIndex( elem => elem.I === ele.I))   
      if(duplicates!==prevState.newdata){
        return { newdata:duplicates };
        console.log(duplicates,"duplicates")
     }
     else return null;      
        // this.setState({details:duplicates})
      
    }
}

componentDidMount(){
  let token=localStorage.getItem("token");
        if(token){
          axios.get(OWNED_DEVICES,
            {headers:{"Authorization":`Token ${token}`}})
            .then(resp=>{
              console.log(resp.data,"owned devices")
              this.setState({devices:resp.data})
            })
        }
}

  render() {
    console.log(this.props,"props") 
    console.log(this.state.devices.length>0,"lenght")    
    const bikeMarkup = renderToStaticMarkup(<img src={bike} width="30px" />);
    const customMarkerBike = divIcon({
      html: bikeMarkup,
      iconSize: [2,2]      
    });
    return (
      <div>   
          <MiniDrawer/>
          
          <div>      
          <LeafletMap
               center={[this.state.lat,this.state.log]}
               draggable={true}
               // position={this.state.lat,this.state.log}
               zoom={13}
               draggable={true}
               maxZoom={19}
               attributionControl={true }
               zoomControl={true}                
               doubleClickZoom={true}
               scrollWheelZoom={true}
               dragging={true}
               animate={true}
               easeLinearity={0.35}
              >
                <TileLayer url="http://{s}.tile.osm.org/{z}/{x}/{y}.png" />              
                {this.state.newdata &&
                         this.state.newdata.map(i=>(
                           <React.Fragment>
                             <Marker
                               key={1}
                              position={[i.La,i.Lo]}                               
                               icon={customMarkerBike}
                               >
                              <Popup>
                                <p>{i.I}</p>
                              </Popup>
                            </Marker>  
                           </React.Fragment>
                         ))
                      }                      
              </LeafletMap></div>
                        
      </div>
    );
  }
}

export default subscribe({
  topic:"apeiron/default"
})(Map);
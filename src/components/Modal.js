import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';
import success from '../assets/success.png';
import Button from '@material-ui/core/Button';


function rand() {
  return Math.round(Math.random() * 20) - 10;
}

function getModalStyle() {
  const top = 50 + rand();
  const left = 50 + rand();

  return {
    top:"50%",
    left:"50%",
    transform: `translate(-${top}%, -${left}%)`,
  };
}

const useStyles = makeStyles((theme) => ({
  paper: {
    position: 'absolute',
    width:"493px",
    height:"285px",   
    backgroundColor: theme.palette.background.paper,
    border: '2px solid #000',
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
    textAlign:"center"
  },
  title:{
      textAlign:"center",
      color:"#34A853",
      fontWeight:"500",
      fontSize:"24px",
      fontFamily:"Roboto"
  },
  img:{
    width:"85px",
    height:"85px",
    textAlign:"center"
  },
  name:{
    color:"#707070",
    letterSpacing:"0px",
    fontFamily:"Roboto",
    fontSize:"18px"
  },
  btn:{
    backgroundColor:"#1A67A3",
    width:"111px",
    height:"40px",
    color:"#FFFFFFDE"
  }
}));

export default function SimpleModal(props) {
  const classes = useStyles();
  // getModalStyle is not a pure function, we roll the style only on the first render
  const [modalStyle] = React.useState(getModalStyle);
  const [open, setOpen] = React.useState(false); 
  const [name,setName]=React.useState(""); 
  const [id,setId]=React.useState("")

  const handleClose = () => {
    setOpen(false);
  };

  React.useEffect(()=>{
      if(props.val===true){
        setOpen(true)
         setName(props.name);
         setId(props.id);
      }
  },[props])

  const body = (
    <div style={modalStyle} className={classes.paper}>
      <h2 id="simple-modal-title" className={classes.title}>The device has been added successfully</h2>
      <p id="simple-modal-description">
        <img src={success} className={classes.img}/>
      </p>
  <p className={classes.name}>Device Name : <span>{name}</span> </p>
  <p className={classes.name}>Device Id : <span>{id}</span> </p>
  <Button variant="contained" className={classes.btn} onClick={e=>setOpen(false)} >
        Ok
      </Button>
      <SimpleModal />
    </div>
  );

  return (
    <div>      
      <Modal
        open={open}
        // onClose={handleClose}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
      >
        {body}
      </Modal>
    </div>
  );
}

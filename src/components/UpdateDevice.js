import React,{useState} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';
import Divider from '@material-ui/core/Divider';
import Backdrop from '@material-ui/core/Backdrop';
import Fade from '@material-ui/core/Fade';
import pin from '../assets/pin.png'
import { Typography,TextInput, TextField } from '@material-ui/core';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import Button from '@material-ui/core/Button';
import Icon from '@material-ui/core/Icon';
import inf from '../assets/inf.svg'
import can from '../assets/can.svg';
// import details from '../../assets/details.svg';
// import Support from '../map/Support'
import Axios from 'axios';  
import SimpleModal from './Modal';
import {VEHICLE_TYPE,UPDATE_DEVICES} from '../constants'



const useStyles = makeStyles((theme) => ({
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  paper: {
    backgroundColor: "#FBFBFB",
    border: '1px solid #707070',
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
    width:"837px",
    height:"328px"
  },
  root: {
    '& > *': {
      margin: theme.spacing(2),
      width: '376px',
      height:"38px",
      
    },
    text1:{
      width:"376px",
      height:"38px",
      color:"red"
    }
     
}
}));

export default function UpdateDevice(props) {
  const classes = useStyles();
  const [open, setOpen] = useState(false);  
  const [id,setId]=useState('');
  const [name,setName]=useState('');
  const [type,setType]=useState('');
  const [show,setShow]=useState(false);
  const [data,setData]=useState([]);
  const [key,setKey]=useState('');

 
  let addDeviceHandler=(e)=>{
    setShow(true)
  e.preventDefault();
  let data={
    "device_id":id,
    "device_name":name,
    "vehicel_type":null
  }
  console.log(data,"data")
  let token=localStorage.getItem("token");
  if(token){
    Axios.get(`${UPDATE_DEVICES}/${key}/`,data,
    {headers:{'Authorization':`Token ${token}`}})
    .then(resp=>{
      console.log(resp.data,"update")
      setOpen(false)
      // setShow(true);
      // setOpen(false)
      // setName(resp.data[0].device_name)
      // setId(resp.data[0].device_id)
      // setData(resp.data)
    })
    .catch(error=>{
      console.log(error)
      alert(error)
    })
  }         
  }
  React.useEffect(()=>{
    if(props.open==true){
        setOpen(true);
        setId(props.ids);
        setName(props.nam)
        setType(props.typ);
        setKey(props.keys)
    }
    Axios.get(VEHICLE_TYPE)
    .then(resp=>{
      console.log(resp.data)
      setData(resp.data)
    })
  },[props])
  return (
    <div>   
       {/* <Support/> */}
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        className={classes.modal}
        open={open}
        // onClose={handleClose}      
        
      >
        <Fade in={open}>
          <div className={classes.paper}>
            <div>
               <img src={pin} width="14px" height="21px"/>
               <span style={{color:"#1A67A3",fontFamily:"Roboto",fontSize:"20px",fontWeight:"bold",marginLeft:"15px"}}>Add Device</span>
            </div>
           <Divider></Divider>           
          <div style={{display:"flex",margin:"10px"}}>
          <div class="form-control" style={{marginRight:"10px"}}>
  <label for="my-input" style={{fontSize:"20px",color:"#707070",fontFamily:"Roboto"}}>Device Id</label><br/>
  <input id="my-input" aria-describedby="my-helper-text" value={id} onChange={e=>setId(e.target.value)} style={{width:"376px",height:"38px",border:"1px solid #BDC3C5",borderRadius:"5px"}} />  
</div>
<div class="form-control"  style={{marginLeft:"25px"}}>
  <label for="my-input" style={{fontSize:"20px",color:"#707070",fontFamily:"Roboto"}}>Device Name</label><br/>
  <input id="my-input" value={name} onChange={e=>setName(e.target.value)} aria-describedby="my-helper-text" style={{width:"376px",height:"38px",border:"1px solid #BDC3C5",borderRadius:"5px"}} />   
</div>
</div>
<div class="form-control" style={{marginLeft:"10px",marginTop:"30px"}}>
  <label for="my-input" style={{fontSize:"20px",color:"#707070",fontFamily:"Roboto"}}>Vehicle Type</label><br/>
  <select name="cars" value={type} onChange={e=>setType(e.target.value)} id="cars" style={{width:"382px",height:"42px",border:"1px solid #BDC3C5",borderRadius:"5px"}}>
    <option value="volvo"></option>
    {data.map(i=>(
      <React.Fragment>
      <option value="saab">{i}</option>    
      </React.Fragment>
    ))}
    
  </select>  
</div>
<div style={{display:"flex",marginTop:"40px"}}>
  <img src={inf} width="20px" height="20px"/>
  <span style={{marginLeft:"5px",color:"#6C6C6C",fontSize:"17px",fontFamily:"Roboto"}}>Please enter your device details</span>
  <button style={{backgroundColor:"#707070",marginLeft:"350px",marginTop:"20px",width:"98px",height:"40px",borderRadius:"5px",color:"#FFFFFF",pointer:"cursor"}}><img src={can} width="12px" height="12px" style={{marginRight:"5px"}} onClick={e=>setOpen(false)}/>Cancel</button>
  <button style={{backgroundColor:"#18749D",marginLeft:"10px",marginTop:"20px",width:"98px",height:"40px",borderRadius:"5px",color:"#FFFFFF"}} onClick={addDeviceHandler}>
    Update Device</button>
</div>
            
          </div>
        </Fade>
      </Modal>     
    </div>
  );
}

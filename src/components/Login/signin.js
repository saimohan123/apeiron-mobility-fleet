import React, { Component } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Typography, Card,TextInput, TextField, responsiveFontSizes,Button } from '@material-ui/core';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import {Link} from 'react-router-dom';
import Grid from '@material-ui/core/Grid';
import './signin.css';
// import backgrd './assets/backgrd.png';
const useStyles = makeStyles((theme) => ({
  root: {
    
  },
  card: {
    padding: theme.spacing(3),
    textAlign: 'center',
    color:'red'
  },
  gtext:{
  fontSize:'18',
  fontFamily: " 'Roboto', sans-serif",
  color:'#000000',
  },
  inbox: {
textAlign:'center',
marginLeft:'28',
color:'red',
backgroundColor:'red'
  }
}));
 export default class Login extends Component {
   
  
  constructor(props) {
    super(props)
    this.state = {
     
    }
  }  
    render() {
        return (
            <div className="main">
              <Grid container>
               <Grid item  xs={4} className="red" justify="right" color="red" spacing="3"></Grid>
        <Grid item justify="center" color="green"  xs={4}>
          
        <div>
                     <Card className="card"> 
                    <form>
                      <div className="logo">
                      </div>
                      <Typography className="Text-input">Welcome back</Typography>
                      <Typography className="sm-txt">Sign In, to continue</Typography>
                      <TextField 
                      type="email"  
                      variant="outlined" 
                      margin="dense" 
                      label="Enter your email"
                      className="inbox">
                      </TextField>
                      <div className="msg"></div>
                      <TextField 
                      id="outlined-basic"
                      type="password"  
                      variant="outlined" 
                      className="box"
                      label="Enter your password"
                      margin="dense"
                      >
                      </TextField>
                      <div className="lck"></div>
                      <div>
                        <Checkbox  className="check" inputProps={{ 'aria-label': 'uncontrolled-checkbox' }} />
                      <Typography className="chck-txt">Remember me</Typography>
                       <Typography className="forgt" >ForgotPassword?</Typography>
                      </div>
                      <Button variant="contained" className="button">
                      signin
                      
                      </Button>
                      <hr className="hr-text" data-content="or"></hr>
                      <Button variant="contained" className="g-btn">
                      <span className="gtext">Google</span>
                      </Button>
                      <Button variant="contained" className="f-btn">
                      facebook
                      </Button>
                      <Typography>
                        <p className="Nw">New user?</p>
                      </Typography>                      
                      <Typography className="acnt"><b>CreateAccount</b></Typography>  
                      </form>
                      </Card> 
                      </div>
                      
        </Grid>
        <Grid item color="blue" xs={4}>
        </Grid>
        </Grid>
                    
                  </div>
       )
   }
}

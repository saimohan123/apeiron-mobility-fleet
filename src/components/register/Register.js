import React, { Component } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Typography, Card, TextInput, TextField, responsiveFontSizes, Button, Divider, CardMedia } from '@material-ui/core';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import { Link } from 'react-router-dom';
import Grid from '@material-ui/core/Grid';
import './Register.css';
import Axios from 'axios'
import BlockIcon from '@material-ui/icons/Block';
import { REG_URL } from '../../constants';
import SimpleModal from '../PopUp';
import { NavLink } from 'react-router-dom';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
// import backgrd './assets/backgrd.png';
const useStyles = makeStyles((theme) => ({
  root: {

  },
  card: {
    padding: theme.spacing(3),
    textAlign: 'center',
    color: 'red'
  },
  gtext: {
    fontSize: '18',
    fontFamily: " 'Roboto', sans-serif",
    color: '#000000',
  },
  inbox: {
    textAlign: 'center',
    marginLeft: '28',
    color: 'red',
    backgroundColor: 'red'
  }
}));
class Register extends Component {


  constructor(props) {
    super(props)
    this.state = {
      email: '', password1: '', password2: '', isRegistered:true, error: ''
    }
  }
  handleChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value
    })
    if (e.target.name == "password1" || e.target.name == "password2" ) {
      let regPass = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$/;
      let passValid = regPass.test(e.target.value);
      if (passValid == false) {
        this.setState({ error: "min 8 char should have 1 upper,lower case and one number" })
        console.log(passValid)
      }
      else {
        this.setState({ error: '' })
      }

    }
  }
  regHandler = (e) => {
    e.preventDefault();
    let data = {
      "email": this.state.email,
      "password1": this.state.password1,
      "password2": this.state.password2
    }
    console.log(data);
    Axios.post(REG_URL, data)
      .then(resp => {
        console.log(resp.data);
        this.setState({
          isRegistered: true
        })
      })
      .catch(error => {
        console.log(error)
        //  alert(error)
        if (error == "Error: Request failed with status code 400") {
          this.setState({ error: "User is already registered with this e-mail address" })
        }
        if (error == "Error: Network Error") {
          this.setState({ error: "check your internet connection" })
        }
      })
  }
  render() {
    return (
      <div className="main">
        <Grid container>
          <Grid item xs={4} className="red" justify="right" color="red" spacing="3"></Grid>
          <Grid item justify="center" color="green" xs={4}>

            <div>
              <Card className="card-r">
                <form>
                  <img
                    className="mail"
                    src={require('./mail.png')}
                  />
                  <Typography className="mail-txt">contact us</Typography>
                  <img
                    className="image"
                    src={require('./logo.png')}
                  />
                  <div className="logo">
                  </div>
                  <Typography className="Text-info">Sign up</Typography>
                  <Typography className="smtxt">Get Started</Typography>
                  <TextField
                    type="email"
                    name="email"
                    value={this.state.email}
                    onChange={this.handleChange}
                    variant="outlined"
                    margin="dense"
                    focused={false}
                    placeholder="Enter your email"
                    className="in-box">
                  </TextField>
                  <div className="msg1"></div>
                  <TextField
                    id="outlined-basic"
                    type="password"
                    name="password1"
                    value={this.state.password1}
                    onChange={this.handleChange}
                    variant="outlined"
                    className="box-1"
                    placeholder="Enter your password"
                    margin="dense"
                    focused={false}
                  >
                  </TextField>
                  <div className="lock"></div>
                  <TextField
                    id="outlined-basic"
                    type="password"
                    name="password2"
                    focused={false}
                    value={this.state.password2}
                    onChange={this.handleChange}
                    variant="outlined"
                    className="box2"
                    placeholder="Confirm your password"
                    margin="dense"
                  >
                  </TextField>
                  <div className="lock"></div>
                  <div>
                    {/* <BlockIcon className="icon"/> */}
                    <Typography className="icon-txt" style={{textAlign:"left",marginLeft:"10px"}}>{this.state.error}</Typography>
                    <Typography className="icn-t"></Typography>
                  </div>
                  <Button variant="contained" className="button-r" onClick={this.regHandler}>
                    sign up
                      </Button>
                  {/* <Divider className="hrtext" />  */}
                  {/* <b className="bold-r">Or</b> */}
                  <Divider className="hrr" />
                  {/* <Button variant="contained" className="gbtn-r">
                      <span className="gtext">Google</span>
                      </Button>
                      <Button variant="contained" className="fbtn-r">
                      facebook
                      </Button>                       */}
                </form>
              </Card>
            </div>

          </Grid>
          <Grid item color="blue" xs={4}>
          </Grid>
        </Grid>
        <div>
        <img className="pur-view" src={require('./purview.png')}/>
        <Typography className="purview-txt">powered by</Typography>
        </div>
        <NavLink to="/" >
          <ArrowBackIcon className="arrow" />
          <Typography className="unknown"> Back  to  Login</Typography>
        </NavLink>
        <SimpleModal reg={this.state.isRegistered} />
      </div>
    )
  }
}
export default (Register);
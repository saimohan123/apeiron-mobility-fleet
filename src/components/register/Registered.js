import React,{useState,useEffect} from 'react';
import SimpleModal from '../PopUp';
import {REG_ACTIVE} from '../../constants';
import Axios from 'axios'

function Registered(props) {
  
    const [reg_active,setReg_active]=useState(false)

    useEffect(() => {
         console.log(props.match.params.key);
         let key=props.match.params.key;
         if(key){
             Axios.post(`${REG_ACTIVE}/${key}/`)
             .then(resp=>{
                 console.log(resp.data);
                 setReg_active(true)
             })
             .catch(error=>{
                 alert(error)
             })
         }
    }, [])
    return (
        <div>            
            <SimpleModal regSuccess={reg_active}/>
        </div>
    )
}

export default Registered
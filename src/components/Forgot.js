import React, { Component } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Typography, Card,TextInput, TextField, responsiveFontSizes,Button, withStyles, CardMedia } from '@material-ui/core';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Grid from '@material-ui/core/Grid';
import './forgot.css'
import Axios from 'axios';
import {FORGOT_CHECK,FORGOT_RESET} from '../constants';
import SimpleModal from './PopUp'
import { NavLink } from 'react-router-dom';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
// import styles from './Forgot.style';
    class Forgot extends Component {
    constructor(props) {
      super(props)
      this.state = {  
          email:'',msg:'',error:'',err:''
      }
    }
    submitHandler=(e)=>{
      e.preventDefault();
         Axios.post(FORGOT_CHECK,{"email":this.state.email})
         .then(resp=>{
             console.log(resp.data);           
             Axios.post(FORGOT_RESET,{"email":this.state.email})
             .then(resp=>{
                 console.log(resp.data)
                 this.setState({msg:true}) 
                 localStorage.setItem("mail",this.state.email)              
             })
           .catch(error=>{
               alert(error)
           this.setState({error:error})
           })
       })
       .catch(error=>{
          //  alert(error)
           if(error=="Error: Request failed with status code 404"){
           this.setState({err:" Please Enter registered email"})            
           }
           if(error=="Error: Network Error"){
            this.setState({err:"check your internet connection"})
          }
       })
    }
      render() {
        // const {classes}=this.props
          return (
              <div className="main">
                <div class="lg"></div>
<Grid container>
    <Grid xs={4}></Grid>
    <Grid xs={4}>
    
      <Card className="sm-card">
      <img
          className="mail"
          src={require('../assets/mail.png')}
        />
        <Typography className="mail-txt">contact us</Typography>
      <img
          className="img1"
          src={require('../assets/logo.png')}
        />
      <Grid xs={4}>
{/* <Typography style={{color:"red"}}>password</Typography> */}
</Grid>
<Grid xs={2}>
<Typography className="pswd">Forgot password</Typography>
</Grid>
<Typography className="content">Please enter your email/phone to reset password</Typography>
<Grid xs={12}>
<TextField className="email" focused={false} onChange={e=>this.setState({email:e.target.value})}   margin="dense" variant="outlined" type="email" placeholder="Enter your Email"  /> 
<div className="msg-im"></div>
</Grid>
<Grid container xs={12}>
          <Typography style={{color:"red",marginLeft:"22px"}}>{this.state.err}</Typography>
</Grid>
<Grid container xs={12}>
<Button variant="contained" className="rst" onClick={this.submitHandler}>
  <p  style={{fontSize:'19px',color:'#FFFFFFDE',marginTop:'17px', textAlign:'center'}}>Reset</p> 
   </Button>
</Grid>
      </Card>
    </Grid>
    <Grid xs={4}></Grid>
</Grid>  
<NavLink to="/" >
          <ArrowBackIcon className="arrow" />
          <Typography className="unknowns"> Back  to  Login</Typography>
        </NavLink>
<div>
        <img className="purviews" src={require('../assets/purview.png')}/>
        <Typography className="purview1-txt">powered by</Typography>
        </div>
<SimpleModal email={this.state.msg} err={this.state.error}/> 

              </div>
          )
      }
    }
    // export default withStyles(styles) (Forgot);
    export default (Forgot);
    
import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';
import success from '../assets/success.png';
import mail from '../assets/mail.png'
import {Link} from 'react-router-dom'
import { Grid } from '@material-ui/core';

function rand() {
  return Math.round(Math.random() * 20) - 10;
}

// function getModalStyle() {
//   const top = 50 + rand();
//   const left = 50 + rand();

//   return {
//     top: `${top}%`,
//     left: `${left}%`,
//     transform: `translate(-${top}%, -${left}%)`,
//   };
// }

const useStyles = makeStyles((theme) => ({
  paper: {
    position: 'absolute',
    top:"233px",
    left:"466px",
    width:"400px",
    height:"230px",
    backgroundColor: theme.palette.background.paper,    
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
    textAlign:"center"
  },
  actv:{
      textAlign:"center",
      color:"#34A853",
      letterSpacing:"0px",
      fontSize:"18px",
      fontWeight:"700"
  },
  img:{
      textAlign:"center"
  },
  text:{
      color:"#34A853",
      fontSize:"16px",
      letterSpacing:"0.54px"
  },
  link:{
      color:"#18749D",
      fontSize:"16px",
      letterSpacing:"0.54px",
      font: "medium 19px roboto",
      cursor:"pointer"
  }
}));

export default function SimpleModal(props) { 

  const classes = useStyles();
  // getModalStyle is not a pure function, we roll the style only on the first render
//   const [modalStyle] = React.useState(getModalStyle);
  const [open, setOpen] = React.useState(false);
  const [title,setTitle]=React.useState("");
  const [reg,setReg]=React.useState("");
  const [login,setLogin]=React.useState(false);
  const [img,setImg]=React.useState(false);


  const handleClose = () => {
    setOpen(false);
  };
   React.useEffect(() => {
       if(props.val){
        setOpen(true); 
        setTitle("Login Successfully...!")
       }
       if(props.email){
        setOpen(true); 
         setTitle("Your password reset link has been sent to your e-email")
         setReg("")
         setImg(true)
       }
       if(props.reg){
         setOpen(true)
         setReg("Your Sign Up is successful.");
         setTitle("Please check your registered email to activate account");
         setImg(true)
       }
       if(props.regSuccess){
         setOpen(true);
         setReg("Your account is activated.");
         setLogin(true)
       }
       if(props.frgt_reset){
         setOpen(true);
         setReg("Your Password is Changed successfully.")
         setLogin(true)        
       }
                  
   }, [props])
  const body = (
    <div  className={classes.paper}>
      <h2 id="simple-modal-title" className={classes.actv}>{reg}</h2>
      <p id="simple-modal-description">
        {img ?
       <img src={mail} width="84px" height="85px" className={classes.img}/>:
       <img src={success} width="84px" height="85px" className={classes.img}/>}
      </p>
      <p className={classes.actv}>{title}</p> 
      {login &&  
      <p className={classes.actv}>Please <Link to="/" style={{textDecoration:"none"}}><span style={{color:"#18749D"}}>click here</span></Link> to login.</p>}       
    </div>
  );
    return (
    <div>  
      <Grid container> 
      <Grid item xs={4}></Grid>      
      <Grid item xs={4}>  
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
      >
        {body}
      </Modal>
      </Grid>
      <Grid item xs={4}></Grid>
      </Grid>
    </div>
  );
}
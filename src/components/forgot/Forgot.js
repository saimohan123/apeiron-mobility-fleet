import React, { Component } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Typography, Card,TextInput, TextField, responsiveFontSizes,Button, withStyles } from '@material-ui/core';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Grid from '@material-ui/core/Grid';
import './forgot.css'
// import styles from './Forgot.style';
    class Forgot extends Component {
    constructor(props) {
      super(props)
      this.state = {  
      }
    }
      render() {
        // const {classes}=this.props
          return (
              <div className="main">
                <div class="lg"></div>
<Grid container>
    <Grid xs={4}></Grid>
    <Grid xs={4}>
      <Card className="sm-card">
<Grid xs={2}>
<Typography className="pswd">Forgot password</Typography>
</Grid>
<Typography className="content">Please enter your email/phone to reset password</Typography>
<Grid xs={12}>
<TextField className="email"  margin="dense" variant="outlined" type="email" label="email" /> 
<div className="msg-im"></div>
</Grid>
<Grid container xs={12}>
<Button variant="contained" className="rst">
  <p  style={{fontSize:'19px',color:'#FFFFFFDE',marginTop:'0px', textAlign:'center'}}>Reset</p> 
   </Button>
</Grid>
      </Card>
    </Grid>
    <Grid xs={4}></Grid>
</Grid>
              </div>
          )
      }
    }
    // export default withStyles(styles) (Forgot);
    export default (Forgot);
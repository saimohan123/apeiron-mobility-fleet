import React,{useEffect, useState} from 'react';
import clsx from 'clsx';
import { fade, makeStyles, useTheme } from '@material-ui/core/styles';
import InputBase from '@material-ui/core/InputBase';
import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import MailIcon from '@material-ui/icons/Mail';
// import Sample from './Sample';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import SearchIcon from '@material-ui/icons/Search';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import './Track.css';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import { Card, Checkbox } from '@material-ui/core';
import { Hidden } from '@material-ui/core';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { NavLink } from 'react-router-dom';
import {OWNED_DEVICES} from '../constants';
import axios from 'axios'
const drawerWidth = 180;

const useStyles = makeStyles((theme) => ({

    root: {
        display: 'flex',
    },
    appBar: {
      background:'#1A67A3',
        left: 0,
        zIndex: theme.zIndex.drawer + 1,
    },
    appBarShift: {

    },
    menuButton: {
        marginRight: 80,
    },
    hide: {
        display: 'none',
    },
    show: {
        display: 'visible'
    },
    // List: {
    //     backgroundColor:'#333739',
    // },
    drawer: {
        width: drawerWidth,
        flexShrink: 0,
        zIndex: 100,
        backgroundColor: 'white',
        border: 'none',
        marginTop: '100px',
        overflow: 'hidden',
        whiteSpace: 'nowrap',
    },
    nav2: {
        right: 916,
        width: 400,
        position: 'absolute',
        height: 600,
        background: 'white',
    },

    nav2Close: {
        right: 1310,
        width: 5,
        position: 'absolute',
        height:500,
        background: 'white',
        whiteSpace:'normal'
    },
    drawerOpen: {
        width: drawerWidth,
        zIndex:'0.9',
        marginTop: '55px',
        overflow: 'hidden',
        backgroundColor:'#333739',
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    drawerClose: {
        overflow: 'hidden',
        marginTop: '60px',
        border: 'none',
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
        overflowX: 'hidden',
        width: theme.spacing(7) + 1,
        [theme.breakpoints.up('sm')]: {
            width: 65,
            backgroundColor:'#333739',
        },
    },
    toolbar: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-end',
        padding: theme.spacing(0, 1),
        ...theme.mixins.toolbar,
    },
    content: {
        flexGrow: 1,
        padding: theme.spacing(3),
    },
    search: {
        position: 'relative',
        borderRadius: theme.shape.borderRadius,
        backgroundColor: fade(theme.palette.common.white, 0.15),
        '&:hover': {
            backgroundColor: fade(theme.palette.common.white, 0.25),
        },
        marginLeft: '40px',
        width: '100%',
        [theme.breakpoints.up('sm')]: {
            marginLeft: theme.spacing(1),
            width: 'auto',
        },
    },
    searchIcon: {
        padding: theme.spacing(0, 2),
        height: '100%',
        paddingLeft: '501px',
        position: 'absolute',
        pointerEvents: 'none',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    inputRoot: {
        border: '1px solid white',
        marginLeft: '90px',
        borderRadius: '5px',
        color: 'inherit',
    },
    inputInput: {
        padding: theme.spacing(1, 1, 1, 0),
        // vertical padding + font size from searchIcon
        paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
        transition: theme.transitions.create('width'),
        width: '100%',
        [theme.breakpoints.up('sm')]: {
            width: '22ch',
            paddingLeft: '10px',
            '&:focus': {
                width: '23ch',
            },
        },
    },
    icon: {
        marginTop:'15px',
        marginLeft:'20px !important',
        width: '20px',
        height: '16px',
      },
      icon1:{
        position:"absolute",
        top:10,
    marginLeft:10,
    width:14,
    height:21,
      },
      icon2:{
        position:"absolute",
        width:13,
        height:18,
        top:-23,
    marginLeft:10,
      },
      star:{
        position:'absolute',
        top:-23,
        left:42,
        height:'6px',
        width:'6px',
      },
      icon3:{
        position:"absolute",
        width:19,
        height:19,
        top:-60,
    marginLeft:7,
      },
      icon4:{
        marginTop:'10px',
        height:'22px',
        width:'22px',
        marginLeft:'15px',
      },
      itext:{
        marginTop:'20px',
        color:'white',
      },
      Card:{
      height:700,
      },
      
      dcon:{
          width:13,
          height:20,
          position:"absolute",
          right:380,
          top:15,
          color:'blue'
      },
      dtxt:{
          fontSize:20,
          color:'#1A67A3',
          right:292,
          top:10,
          position:"absolute",
      },
     searchIcons:{
       position:"absolute",
       color:'grey',
       right:10,
       top:-5,
     },
     inputRoots:{
       top:-10,
       left:100,
       
     },
     inputInputs:{
      //  position:"absolute",
       top:0,
       left:850,
       border:'1px solid #626262',
       borderRadius:5,
     },
    dtext1:{
      color:'#666666',
      fontSize:'12px',
      fontFamily:'roboto',
      marginLeft:'-5px',
    },
    dtext2:{
color:'#666666',
fontSize:'12px',
    },
     user:{
marginLeft:'88%',
     },
     bx:{
       left:'15px',
      position:'relative'
     },
 filter:{
   position:"relative",
  top:19,
  right:'5px',
},
bike:{
  right:155,
  top:20,
},
more:{
  top:'2px',
  left:'-20px',
  position:'relative',
  color:'#6C6C6C'
}
}));

export default function MiniDrawer() {
    const classes = useStyles();
    const theme = useTheme();
    const [open, setOpen] = React.useState(false);
    const [navOpen, setNavOpen] = React.useState(false);
    const [secondNavClass, setSecondNavClass] = React.useState(classes.nav2Close);
    const [data,setData]=React.useState([])

    const handleDrawer = () => {
        if (open == false) {
            setOpen(true);
        } else {
            setOpen(false);
        }
    };

    const secondHandleDraweropen = () => {
        if (navOpen == false) {
            setSecondNavClass(classes.nav2)
            setNavOpen(true);

        } else {
            setSecondNavClass(classes.nav2Close)
            setNavOpen(false);
        }
    };
    const handleDrawerOpen = () => {
        setOpen(true);
      };
      const handleDrawerClose = () => {
        setOpen(false);
      };
      useEffect(() => {
        let token=localStorage.getItem("token");
        if(token){
          axios.get(OWNED_DEVICES,
            {headers:{"Authorization":`Token ${token}`}})
            .then(resp=>{
              console.log(resp.data,"owned devices")
              setData(resp.data)
            })
        }
      }, [])
      const [showText, setShowText] = useState(true);
      const [showIcon, setShowIcon] = useState(false)
    return (
        <div className={classes.root}>
            <CssBaseline />
            <AppBar
                position="fixed"
                className={clsx(classes.appBar, {
                    [classes.appBarShift]: open,
                })}
            >
                <Toolbar>
                    <IconButton
                        color="inherit"
                        aria-label="open drawer"
                        onClick={handleDrawer}
                        edge="start"
                        className={clsx(classes.menuButton, {
                            [classes.show]: open,
                        })}>
                        <MenuIcon onClick={() => setShowText(!showText)} />
                    </IconButton>
                    <img className='title' src={require('../assets/logo.png')} />
                    {/* <div className={classes.searchIcon}>
                        <SearchIcon />
                    </div>
                    <InputBase
                        placeholder="Search…"
                        classes={{
                            root: classes.inputRoot,
                            input: classes.inputInput,
                        }}
                        inputProps={{ 'aria-label': 'search' }}
                    /> */}
                    <div>
                        <Typography className='text'>John&nbsp;Jacobs</Typography>
                    </div>
                    <div className={classes.user}>
                        <AccountCircleIcon />
                    </div>
                    <div className={classes.drop}>
                        <ArrowDropDownIcon />
                    </div>

                </Toolbar>
            </AppBar>
            <div>
            <Drawer
        variant="permanent"
        className={clsx(classes.drawer, {
          [classes.drawerOpen]: open,
          [classes.drawerClose]: !open,
        })}
        classes={{
          paper: clsx({
            [classes.drawerOpen]: open,
            [classes.drawerClose]: !open,
          }),
        }}
      >
        
        <Divider />
        <div>
         
        <List>
        
            <ListItem button >
              <ListItemIcon>
                <img  className={classes.icon1} src={require('../assets/location.png')}/><br/>
                {showText &&
          <div>
            <ListItemText> 
        <h1 className="trc">Tracking</h1>
              </ListItemText>

          </div>}
        </ListItemIcon>
              <ListItemText> 
        <h1 className="ic-t3">Tracking</h1>
              </ListItemText>
            </ListItem>
            <ListItem button >
              <ListItemIcon>
                <NavLink to="/AddDevices">
              <img className={classes.icon2} src={require('../assets/loc.png')}/>
        <img className={classes.star} src={require('../assets/star.png')}/></NavLink>
        {showText &&
          <div>
            <ListItemText> 
        <h1 className="advc">Add Device</h1>
              </ListItemText>

          </div>}
        </ListItemIcon>
              <ListItemText> 
              <h1 className="ic-t4 modal-trigger">Add device</h1>
              </ListItemText>
            </ListItem>
            <ListItem button >
              <ListItemIcon>
              <NavLink to="/Viewsales"><img className={classes.icon3} href="./Viewsales" src={require('../assets/Admin.png')}/></NavLink>
              {showText &&
          <div>
            <ListItemText> 
        <h1 className="adm">Admin</h1>
              </ListItemText>

          </div>}
        </ListItemIcon>
              <ListItemText> 
              <h1 className="ic-t5">Admin</h1>
              </ListItemText>
            </ListItem>
            <div>           
              <ListItem>
            <ListItemIcon>
            {/* <img className="purview" src={require('../assets/purview.png')}/> */}
            </ListItemIcon>
            </ListItem>
            </div>
 
        </List>
        </div>
      </Drawer>
            
                <div className={secondNavClass}>
                        <Card>
                          <div>                         
                        <img className={classes.dcon} src={require('../assets/pin.png')}/>
                        <b className={classes.dtxt}>Tracking</b>
                        <img className={classes.filter} src={require('../assets/fill.png')}/>
                        <div className={classes.search}>
            <div className={classes.searchIcons}>
              <SearchIcon />
            </div>
            <InputBase
              placeholder="Search…"
              classes={{
                root: classes.inputRoots,
                input: classes.inputInputs,
              }}
              inputProps={{ 'aria-label': 'search' }}
            />
  
          </div>
           <div>
           <Paper  style={{ width: "100%" }}>
             <TableContainer style={{width:'445px',overflow:'hidden'}}>
             <Table stickyHeader aria-label="sticky table">
             <TableHead>
                    <TableRow>
                      <TableCell style={{background:'white'}}>
           <Typography className={classes.dtext1}>List of Devices</Typography>
           </TableCell>
           <TableCell style={{background:'white',right:'55px'}}>
            <Typography className={classes.dtext2} >Map</Typography>
            </TableCell>
            <TableCell></TableCell>
            <TableCell></TableCell>
            <TableCell></TableCell>
            </TableRow>
            </TableHead>
            {data.length>0 ?
            <div>
              {data.map(i=>(
                <React.Fragment>
                  {i.relations.map(z=>(
                    <React.Fragment>
                      <TableBody>
                        <TableRow>
                          <TableCell>
            <img  src={require('../assets/car.png')}/>
            </TableCell>
            <TableCell style={{right:'20px',position:'relative',fontSize:'14px',color:'#000000'}}>
                  <b>{z.device_id}</b><br/>
                  </TableCell>
                  <TableCell className={classes.bx}>
           
            </TableCell>
            <TableCell></TableCell>
            <TableCell></TableCell>
            <TableCell> 
              <Checkbox className={classes.bx} inputProps={{ 'aria-label': 'controlled-checkbox' }} />
              </TableCell>
            <TableCell>
            <MoreVertIcon  className={classes.more}/>
            </TableCell>
            </TableRow>
            </TableBody>
                   </React.Fragment>
                  ))}
                      {/* <img className={classes.bike} src={require('../assets/bike.png')}/>
            <b className={classes.dtext5}>Device Id/Name</b>
            <Checkbox color="green"className={classes.bx}/>
            <MoreVertIcon className={classes.more}/>
            <Divider/> */}
           
                </React.Fragment>
              ))}            
            </div>:<div>
              <p style={{textAlign:"left",color:"#FF0000",fontSize:"19px"}}>No devices to display,Please add device(s)</p>
              </div>}
              </Table>
              </TableContainer>
              </Paper>
           </div>

           </div>
           {showIcon &&
           <div style={{width:20,background:'white',position:'absolute',height:'50px', left:400,top:'45%',borderRadius:'5px'}}>
           <IconButton style={{ right: 13 }}  onClick={secondHandleDraweropen}>
                             <ChevronLeftIcon />
                        </IconButton>
           </div>}
                     </Card>
                    </div>
                    <div style={{ left: 65, top: '50%', width: 20, position: 'absolute', height: '50px', background: 'white', borderRadius: '5px' }}>
                        <IconButton style={{ right: 10 }}  onClick={secondHandleDraweropen}>
                     <ChevronRightIcon onClick={() => setShowIcon(!showIcon)}/>
                        </IconButton>

                    </div>

                

            </div>
            {/* <main className={classes.content}>
                <div className={classes.toolbar} />


                <Sample />

            </main> */}
        </div>
    );
}


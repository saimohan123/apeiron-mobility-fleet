import React from 'react';
import Login from './components/signin';

import Register from './components/Register/Register';
import Registered from './components/Register/Registered';
import Forgot from'./components/Forgot';
import Forgot1 from './components/ForgotReset'
import './App.css';
import {BrowserRouter,Route,Switch} from 'react-router-dom'
import MiniDrawer from './components/Tracking';
import Support from './components/Support'
import AddDevices from './components/AddDevices'
import ViewSales from './components/Admin/Admin';
import Accountmanage from './components/Admin/Admin2';
import Track from './components/Track';

function App() {
  return (
    <BrowserRouter>
    <div className="App">
      <Switch>
      <Route path="/ViewSales" exact component={ViewSales}/>   
      <Route path="/" exact component={Login}/> 
      <Route path='/register' component={Register}/>
      <Route path="/account-confirm-email/:key/" component={Registered}/>
      <Route path="/forgot" component={Forgot}/>
      <Route path="/password-reset/confirm/:uid/:token/" component={Forgot1}/>
      <Route path="/MiniDrawer" component={MiniDrawer}/>
      <Route path="/map" component={Support}/>
      <Route path="/adddevices" component={AddDevices}/>
      <Route path="/Accountmanage" component={Accountmanage}/>
      <Route path="/track" component={Track}/>
      </Switch>
    </div>
    
    </BrowserRouter>
  );
}

export default App;
export const BASE_URL="https://apeiron-mobility.herokuapp.com";
export const LOGIN_URL=`${BASE_URL}/rest-auth/login/`;
export const REG_URL=`${BASE_URL}/rest-auth/registration/`;
export const REG_ACTIVE=`${BASE_URL}/registration/account-confirm-email`;
export const FORGOT_CHECK=`${BASE_URL}/emailcheck/`;
export const FORGOT_RESET=`${BASE_URL}/rest-auth/password/reset/`
export const FORGOT_CONFIRM=`${BASE_URL}/rest-auth/password/reset/confirm/`;
export const USER_ID=`${BASE_URL}/user-id`;
export const OWNED_DEVICES=`${BASE_URL}/owned-devices/`;
export const LIST_DEVICES=`${BASE_URL}/add-list-devices/`;
export const VEHICLE_TYPE=`${BASE_URL}/vehicletype/`;
export const CREATE_DEVICES=`${BASE_URL}/create-devices/`;
export const UPDATE_DEVICES=`${BASE_URL}/update-devices`


